﻿using OfficeOpenXml;
using System;
using System.IO;

namespace EPPlus_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = @"teste.xlsx";

            using (var package = new ExcelPackage(new FileInfo(url)))
            {
                ExcelWorkbook workbook = package.Workbook;
                ExcelWorksheet sheet = workbook.Worksheets["sheet_2"];

                sheet.Cells["L1"].Formula = "MATCH(\"maria\";G1:G4;0)";
                sheet.Cells["L1"].Calculate();

                Console.WriteLine("--> Match - string: linha {0} ", sheet.Cells["L1"].Value);

                sheet.Cells["L1"].Formula = "INDEX(G1:G4;MATCH(\"pedro\";G1:G4;0))";
                sheet.Cells["L1"].Calculate();

                Console.WriteLine("--> Match: {0} ", sheet.Cells["L1"].Value);
            }
        }
    }
}